#!/usr/bin/env python3
from PIL import Image, ImageMath
import numpy

def colourLandSea(to_colour):
	colours_ref = {"blue":(0,0,230),"red":(230,0,0),"green":(0,230,0),"yellow":(230,230,0),"violet":(230,0,230),"gray":(80,80,80)}

	im = Image.open(to_colour["country"]+".png")
	im = im.convert("RGBA")

	data = numpy.array(im)
	r,g,b,a = data.T
	
	land = (r == 255) & (g == 255) & (b == 255)
	data[..., :-1][land.T] = colours_ref[to_colour["colour"]]

	im_coloured = Image.fromarray(data)
	if to_colour["land"]:
		token = Image.open("../army.png")
		position = (162,175)
	else:
		token = Image.open("../fleet.png")
		position = (236,415)	
	im_coloured.paste(token,position,token)
	
	return im_coloured

if __name__ == "__main__":

	start_sit = [{"country":"EDI","colour":"blue","land":True},{"country":"TYS","colour":"yellow","land":False}]
	countries = list()
	for start_country in start_sit:
		countries.append(colourLandSea(start_country))

	board = Image.open("map.png")
	text = Image.open("text_shaded.png")

	board = board.convert("RGBA")
	text = text.convert("RGBA")

	for country in countries:
		board.paste(country,(0,0),country)
	board.paste(text,(0,0),text)
	board.show()
